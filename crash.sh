#!/bin/bash

if [ -d /etc/mysql ];
then
    CONF=/etc/mysql/conf.d/crash.cnf
fi


if [ -d /etc/my.cnf.d ];
then
    CONF=/etc/my.cnf.d/crash.cnf
fi

echo "Stopping MariaDB to prepare the crash"
service mysql stop

echo "Giving you a random crash out of the big hat"

MEMAVAILABLE=`cat /proc/meminfo |grep MemFree|awk '{print $2}'`
MEMAVAILABLE=${MEMAVAILABLE#0}
MEMBUFFERPOOL=$(($MEMAVAILABLE / 10 * 9))

echo "[mysqld]" > $CONF
echo "innodb_buffer_pool_size=${MEMBUFFERPOOL}k" >> $CONF

service mysql start

mysql crash < oom/init.sql

while [ 1 ];
do
    mysql crash < oom/run.sql > /dev/null
done

